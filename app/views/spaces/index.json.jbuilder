json.array!(@spaces) do |space|
  json.extract! space, :id, :district, :location, :address, :accomodates, :amenities, :start_time, :end_time, :rate
  json.url space_url(space, format: :json)
end
