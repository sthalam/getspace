class CreateSpaces < ActiveRecord::Migration
  def change
    create_table :spaces do |t|
      t.string :district
      t.string :location
      t.text :address
      t.integer :accomodates
      t.text :amenities
      t.time :start_time
      t.time :end_time
      t.float :rate

      t.timestamps null: false
    end
  end
end
